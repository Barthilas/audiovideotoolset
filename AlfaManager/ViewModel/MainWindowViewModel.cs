﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlfaManager.Commands;
using Microsoft.Win32;
using Emgu.CV;

namespace AlfaManager.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Variables
        private string videoFileName;
        private VideoCapture videoCapture;
        private int noFrames;
        private char[] segmentIdent;
        #endregion

        #region Properties
        public SimpleCommand OpenFile { get; set; }
        public string VideoFileName { get => videoFileName; set => videoFileName = value; }
        public VideoCapture VideoCapture { get => videoCapture; set => videoCapture = value; }
        public int NoFrames { get => noFrames; set => noFrames = value; }
        public char[] SegmentIdent { get => segmentIdent; set => segmentIdent = value; }
        #endregion


        #region Methods
        public MainWindowViewModel()
        {
            OpenFile = new SimpleCommand(OpenFileExecute);
        }
        private void OpenFileExecute()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Video files (*.mp4)|*.mp4|Video files (*.avi)|*.avi|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                videoFileName = openFileDialog.FileName.Substring(openFileDialog.FileName.LastIndexOf("\\") + 1, openFileDialog.FileName.LastIndexOf(".") - openFileDialog.FileName.LastIndexOf("\\") - 1);
                int i;
                videoCapture = new VideoCapture(openFileDialog.FileName);
                NoFrames = (int)videoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount);
                SegmentIdent = new char[NoFrames];
                for (i = 0; i < NoFrames; i++) SegmentIdent[i] = ' ';
                //showImagesStart();
            }
        }


        #endregion


        public void ChangedProperty(string nazevVlastnosti)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nazevVlastnosti));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
